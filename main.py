from utils import (
    concatenate_words_with_random_digits,
    get_random_words,
    get_words_amount,
)


def main() -> str:
    """Generate string with random words from vocabulary and mandatory random integers between them."""
    words_amount = get_words_amount()
    random_words = get_random_words(words_amount=words_amount)
    string = concatenate_words_with_random_digits(words=random_words)
    print(string)
    return string


if __name__ == "__main__":
    main()

from unittest.mock import MagicMock

import pytest
from pytest_mock.plugin import MockerFixture
from utils import (
    concatenate_words_with_random_digits,
    get_random_words,
    get_words_amount,
)


WORDS_AMOUNT = 2


@pytest.fixture
def secrets_mocked(mocker: MockerFixture) -> MagicMock:
    """Create a MagicMock object that mimics function from secrets in utils.py."""
    secrets_mocked = mocker.patch("utils.secrets")
    secrets_mocked.SystemRandom.return_value.sample.return_value = ["a", "b", "c"]
    secrets_mocked.SystemRandom.return_value.sample.return_value = ["a", "b", "c"]
    secrets_mocked.randbelow.side_effect = [1, 22, 333]
    return secrets_mocked


def test_get_random_words(secrets_mocked: MagicMock) -> None:
    """Test test_get_random_words function."""
    assert get_random_words() == ["a", "b", "c"]


def test_concatenate_words_with_random_digits(secrets_mocked: MagicMock) -> None:
    """Test that concatenate_words_with_random_digits function concatenates words with random digits between them."""
    assert concatenate_words_with_random_digits(words=["a", "b", "c"]) == "a1b22c333"


@pytest.fixture
def argparse_mocked(mocker: MockerFixture) -> MagicMock:
    """Create a MagicMock object that mimics argparse module in utils.py."""
    argparse_mocked = mocker.patch("utils.argparse")
    args = mocker.MagicMock()
    args.words_amount = WORDS_AMOUNT
    argparse_mocked.ArgumentParser.return_value.parse_args.return_value = args
    return argparse_mocked


def test_get_words_amount(argparse_mocked: MagicMock) -> None:
    """Test that get_words_amount returns number that passed to argparse."""
    assert get_words_amount() == WORDS_AMOUNT

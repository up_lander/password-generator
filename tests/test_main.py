from unittest.mock import MagicMock

import pytest
from pytest_mock.plugin import MockerFixture

from main import main


@pytest.fixture(autouse=True)
def concatenate_words_with_random_digits_mocked(mocker: MockerFixture) -> MagicMock:
    """Create a MagicMock object that mimics a concatenate_words_with_random_digits function."""
    return mocker.patch("main.concatenate_words_with_random_digits", return_value="a123b")


@pytest.fixture(autouse=True)
def get_words_amount_mocked(mocker: MockerFixture) -> MagicMock:
    """Create a MagicMock object that mimics a get_words_amount function."""
    return mocker.patch("main.get_words_amount", return_value=2)


def test_main() -> None:
    """Test that main returns result of concatenate_words_with_random_digits without changes."""
    assert main() == "a123b"

# Password Generator

Generate password from random words from vocabulary and mandatory random numbers between them.

## Prerequisites
Before setting up the project, make sure you have the following dependencies installed on your system:

- `Python 3.11+`
- `poetry 1.5+`

## Setup
- `create virtualenv`
- `poetry install --no-interaction`

## Examples
To generate new password, execute the following command:
```bash
python main.py --words-amount=<WORDS_AMOUNT>
```
Replace `<WORDS_AMOUNT>` with the actual words amount you want to choose. Default is 5.

To generate 5 words as default, execute the following command

```bash
python main.py
```

To generate 2 words as default, execute the following command

```bash
python main.py --words-amount=2
```

## CI
For CI isort, black and ruff are used in gitlab ci. For more information see `.pre-commit-config.yaml`
and `pyproject.toml`

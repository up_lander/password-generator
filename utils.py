import argparse
import secrets
from pathlib import Path


WORDS_AMOUNT = 5
MAX_WORDS_AMOUNT = 100
MAX_NUMBER = 10000
BASE_PATH = Path(__file__).parent.resolve()
PATH_TO_VOCABULARY = BASE_PATH / "vocabulary"


def get_words_amount() -> int:
    """Get words amount from '--words-amount' argument passed to script.

    Returns:
        Amount of words passed to script with the argument '--words-amount'.
    """
    parser = argparse.ArgumentParser(description="Process command-line arguments")
    parser.add_argument("--words-amount", default=WORDS_AMOUNT, type=int, help="The amount of words")
    args = parser.parse_args()
    return args.words_amount


def get_random_words(
    path_to_vocabulary: str = PATH_TO_VOCABULARY,
    words_amount: int = WORDS_AMOUNT,
) -> list[str]:
    """Get random words from vocabulary list.

    Args:
        path_to_vocabulary: a path to vocabulary.
        words_amount: an amount of words to random.

    Returns:
        A list with random words.
    """
    if words_amount <= 0 or words_amount >= MAX_WORDS_AMOUNT:
        error_message = f"Words amount should be greater than 0 and less or equal than {MAX_WORDS_AMOUNT}"
        raise RuntimeError(error_message)
    with Path.open(path_to_vocabulary) as vocabulary:
        words = [word.strip() for word in vocabulary.readlines()]
        return secrets.SystemRandom().sample(words, words_amount)


def concatenate_words_with_random_digits(
    words: list[str],
    max_number: int = MAX_NUMBER,
) -> str:
    """Concatenate string with random digits as separator.

    Args:
        words: words to concatenate.
        max_number: max number to random.

    Returns:
        A new string with random numbers.
    """
    string = ""
    for word in words:
        string += word + str(secrets.randbelow(max_number))
    return string
